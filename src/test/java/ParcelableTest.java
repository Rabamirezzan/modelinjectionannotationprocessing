//import com.modelinjection.injection.ModelInjection;
//import com.modelinjection.injection.ParcelableInterfaceGenerator;
//import com.modelinjection.injection.data.ModelData;
//import com.sun.codemodel.*;
//import com.sun.codemodel.writer.SingleStreamCodeWriter;
//import org.junit.Assert;
//import org.junit.Test;
//
//import java.io.ByteArrayOutputStream;
//import java.io.IOException;
//import java.lang.annotation.Annotation;
//import java.util.List;
//
///**
// * Created by rramirezb on 05/01/2015.
// */
//public class ParcelableTest {
//
//
//
//        @Test
//        public void test(){
//            int expected = 0;
//            JCodeModel codeModel = new JCodeModel();
//            try {
//                JDefinedClass _cls = codeModel._class("com.test.HolaMundo");
//                JClass cls = codeModel.ref(List.class).narrow(MyParcelable.class);
//                _cls.field(JMod.PRIVATE,cls, "myFieldList");
//                _cls.field(JMod.PUBLIC, MyParcelable.class, "field");
//
//
//                ModelData modelData= new ModelData();
//                modelData.setImplementationPackage("a.b.c");
//                modelData.setModelInjectionAnn(new ModelInjection(){
//                    @Override
//                    public Class<? extends Annotation> annotationType() {
//                        return ModelInjection.class;
//                    }
//
//                    @Override
//                    public boolean generatedId() {
//                        return true;
//                    }
//
//                    @Override
//                    public String propertyId() {
//                        return "";
//                    }
//
//                    @Override
//                    public String defaultPropertyType() {
//                        return "java.lang.Integer";
//                    }
//
//                    @Override
//                    public String implementationSuffix() {
//                        return "Impl";
//                    }
//
//                    @Override
//                    public boolean foreignAutoRefresh() {
//                        return false;
//                    }
//                });
//                ParcelableInterfaceGenerator.generateImplementation(_cls, modelData, codeModel);
////                ParcelableInterfaceGenerator.generateParcelableConstructor(_cls, codeModel);
////                ParcelableInterfaceGenerator.generateCREATOR(_cls, codeModel);
//                SingleStreamCodeWriter  w;
//                ByteArrayOutputStream out = new ByteArrayOutputStream();
//                w = new SingleStreamCodeWriter(out);
//                codeModel.build(w);
//                String result = out.toString();
//                System.out.println(result);
//
//            } catch (JClassAlreadyExistsException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            Assert.assertEquals("", expected, expected);
//        }
//
//}
