



import android.os.Parcel;
import android.os.Parcelable.Creator;

import java.util.List;

public class HolaMundo {

    private List<MyParcelable> myFieldList;
    public MyParcelable field;
    public final static android.os.Parcelable.Creator<HolaMundo> CREATOR = new Creator<HolaMundo>() {


        public HolaMundo createFromParcel(Parcel in) {
            return new HolaMundo(in);
        }

        public HolaMundo[] newArray(int size) {
            return new HolaMundo[size] ;
        }

    }
            ;

    public HolaMundo() {
    }

    public HolaMundo(Parcel in) {
        readFromParcel(in);
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeParcelable(this.field, flags);
    }

    public void readFromParcel(Parcel in) {
        this.field = in.readParcelable(this.getClass().getClassLoader());
    }

    public int describeContents() {
        return (0);
    }

}
