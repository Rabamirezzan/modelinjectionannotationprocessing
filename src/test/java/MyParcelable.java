import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by rramirezb on 05/01/2015.
 */
public class MyParcelable implements Parcelable {

    public final static android.os.Parcelable.Creator<MyParcelable> CREATOR = new Creator<MyParcelable>() {


        public MyParcelable createFromParcel(Parcel in) {
            return new MyParcelable(in);
        }

        public MyParcelable[] newArray(int size) {
            return new MyParcelable[size] ;
        }

    }
            ;

    public MyParcelable(Parcel in) {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

    }
}
