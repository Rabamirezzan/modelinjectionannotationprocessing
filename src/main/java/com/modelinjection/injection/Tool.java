package com.modelinjection.injection;

import com.codeinjection.tools.CodeModelTool;
import com.codeinjection.tools.ElementTool;
import com.j256.ormlite.field.DatabaseField;
import com.modelinjection.injection.data.ModelData;
import com.sun.codemodel.*;
import com.sun.codemodel.writer.SingleStreamCodeWriter;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.VariableElement;
import javax.tools.FileObject;
import javax.tools.JavaFileObject;
import javax.tools.StandardLocation;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Set;

import static com.codeinjection.tools.ElementTool.toUpperFirstLetter;

/**
 * Created by rramirezb on 26/12/2014.
 */
public class Tool {


    public static void build(JCodeModel codeModel, Writer writer) {
        if (codeModel != null && writer != null) {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            try {
                codeModel.build(new SingleStreamCodeWriter(out));
                String code = out.toString().replaceAll("^[^\n]*-+[^\n]*-+[^\n]*\n|^\n$", "");
                writer.append(code);
                writer.flush();
                writer.close();
            } catch (Exception e) {

                e.printStackTrace();
            }
        }
    }

    public static Writer generateClass(ProcessingEnvironment processingEnv, String qualifiedNameClass) {
        Writer writer = null;
        try {

            JavaFileObject source = processingEnv.getFiler().createSourceFile(qualifiedNameClass);
            writer = source.openWriter();
        } catch (Exception e) {

            e.printStackTrace();
        }
        return writer;
    }

    public static Writer getOutputLog(ProcessingEnvironment processingEnv){
        FileObject file = null;
        Writer writer = null;
        try {
            file= processingEnv.getFiler().createResource(StandardLocation.SOURCE_OUTPUT, "", "output_log.txt");
            writer= file.openWriter();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return writer;
    }

    public static void println(Writer writer, String format, Object... args){
        try {
            if(writer!=null)
            writer.append(String.format(format+"\n", args));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void close(Writer writer){
        try {
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    public static String getMethodName(ExecutableElement executableElement) {
        return executableElement.getSimpleName().toString();
    }

    public static boolean isGetMethod(String method) {
        return method.toLowerCase().startsWith("get");
    }

    public static boolean isGetMethod(ExecutableElement method) {
        return isGetMethod(method.getSimpleName().toString())||method.getParameters().isEmpty();
    }



    public static boolean isGetMethod(JMethod method) {
        return isGetMethod(method.name());
    }

    protected static String getStacktrace(Throwable e) {
        ByteArrayOutputStream array = new ByteArrayOutputStream();
        String result;
        try {
            array = new ByteArrayOutputStream();
            PrintWriter writer = new PrintWriter(array);
            e.printStackTrace(writer);
            writer.flush();
            writer.close();
            result = array.toString();
        } catch (Exception e1) {
            result = e1.getMessage();
            e1.printStackTrace();
        }
        return result;
    }

    public static String getSetMethodName(JFieldVar property, boolean getMethodExists) {
        String name = getMethodExists?"set"+toUpperFirstLetter(property.name()):property.name();
        return name;
    }

    public static String getSetMethodName(String property) {
        return "set"+toUpperFirstLetter(property);
    }

    public static String getGetMethodName(String property) {
        return "get"+toUpperFirstLetter(property);
    }

    public static void generateId(JDefinedClass cls, String idType, ModelInjection modelInjectionAnn, JCodeModel codeModel) {
        JClass _idType = codeModel.ref(idType);
        JFieldVar id = cls.field(JMod.PRIVATE, _idType, "_Id");
        if(modelInjectionAnn.generatedId()) {
            id.annotate(DatabaseField.class).param("generatedId", true);
        }else{
            id.annotate(DatabaseField.class).param("id", true);
        }
        JMethod setMethod = cls.method(JMod.PUBLIC, void.class, modelInjectionAnn.suppressPOJOPrefixes()?"_Id":"set_Id");
        JVar paramId = setMethod.param(id.type(), "id");
        setMethod.body().assign(JExpr.refthis("_Id"), paramId);
        JMethod getMethod = cls.method(JMod.PUBLIC, _idType, modelInjectionAnn.suppressPOJOPrefixes()?"_Id":"get_Id");
        //getMethod.param(id.type(), "id");
        getMethod.body()._return(JExpr.refthis("_Id"));
    }

    public static void generateId(JDefinedClass cls, String idType, JCodeModel codeModel) {
        JClass _idType = codeModel.ref(idType);
        JFieldVar id = cls.field(JMod.PRIVATE, _idType, "_Id");

            id.annotate(DatabaseField.class).param("generatedId", true);



        JMethod setMethod = cls.method(JMod.PUBLIC, void.class, "set_Id");
        JVar paramId = setMethod.param(id.type(), "id");
        setMethod.body().assign(JExpr.refthis("_Id"), paramId);
        JMethod getMethod = cls.method(JMod.PUBLIC, _idType, "get_Id");
        //getMethod.param(id.type(), "id");
        getMethod.body()._return(JExpr.refthis("_Id"));
    }

    public static boolean isForeign(JFieldVar property, Set<ModelData> interfaces) {
        boolean foreign= false;

        foreign = isForeign(CodeModelTool.getTypeWithoutTypeParameters(property.type().fullName()), interfaces);
        return foreign;

    }

//    public static boolean isForeign(JFieldVar property, Set<VariableElement> interfaces) {
//        boolean foreign = false;
//        foreign = isForeign(getTypeWithoutTypeParameters(property.type().fullName()), interfaces);
//        return foreign;
//    }

    public static boolean isForeign(String propertyType, Set<ModelData> interfaces) {
        boolean foreign = false;
        for(ModelData i:interfaces){
            if(i.getElement().getQualifiedName().toString().equals(CodeModelTool.getTypeWithoutTypeParameters(propertyType))){
                foreign = true;
            }
        }
        return foreign;
    }

    public static String getImplementedName(String implementationPackage, String interfaceName, ModelInjection modelInjectionAnn) {
        return ElementTool.getImplementedName(implementationPackage, "", interfaceName, modelInjectionAnn.classParams().classSuffix());
    }





    public static boolean isOrmDataType(String qualifiedName){
        boolean result = false;
        return result;
    }





    public static void generateDefaultConstructor(JDefinedClass cls){
        cls.constructor(JMod.PUBLIC);
    }

    public static void generateConstructorFields(JDefinedClass cls,  JFieldVar... fields){
        JMethod constructor = cls.constructor(JMod.PUBLIC);
        for(JFieldVar field: fields){
            JVar param = constructor.param(field.type(), field.name());
            constructor.body().assign(JExpr.refthis(field.name()), param);
        }
    }

    public static void generateConstructorFields(JDefinedClass cls){
        JFieldVar[] fields = cls.fields().values().toArray(new JFieldVar[]{});
        generateConstructorFields(cls, fields);
    }

    public static boolean containsGetPrefix(ExecutableElement executableElement) {
        return executableElement.getSimpleName().toString().toLowerCase().startsWith("get");
    }


    public static String getImplementationPackage(ModelData modelData) {
        String name = "";
        String p = modelData.getAnnotation().classParams().classPackage();
        if(p!=null&& !p.trim().equals("")){
            name = p;
        }else{
            if(modelData.getElement().equals(modelData.getAnnotationOwner())){
                name = ElementTool.getImplementationPackage(modelData.getElement());
            }else{
                name = ElementTool.getImplementationPackage((VariableElement)modelData.getAnnotationOwner());
            }
        }
        return name;
    }
}
