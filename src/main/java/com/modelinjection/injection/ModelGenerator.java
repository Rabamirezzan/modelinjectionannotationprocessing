package com.modelinjection.injection;

import com.codeinjection.tools.CodeModelTool;
import com.codeinjection.tools.ElementTool;
import com.codeinjection.tools.ValidDataTypes;
import com.codeinjection.tools.data.DataElementExtractor;
import com.j256.ormlite.field.DatabaseField;
import com.modelinjection.injection.data.ModelData;
import com.ormliteinjection.core.db.injection.DaoInjection;
import com.ormliteinjection.core.db.injection.GeneratedCode;
import com.parcelableandroid.injection.ParcelableInterfaceGenerator;
import com.sun.codemodel.*;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.util.ElementFilter;
import javax.tools.Diagnostic;
import java.io.IOException;
import java.io.Writer;
import java.util.*;

/**
 * Created by rramirezb on 26/12/2014.
 */
public class ModelGenerator {

    public static void generate(ProcessingEnvironment processingEnv, Set<? extends Element> elements) {

        log = Tool.getOutputLog(processingEnv);
        try {
            Set<VariableElement> fields = ElementFilter.fieldsIn(elements);
            Tool.println(log, "By VariableElements: %s", fields);
            generateByVariableElements(fields, processingEnv);

            Tool.println(log, "By TypeElements: %s", fields);
            Set<TypeElement> types = ElementFilter.typesIn(elements);
            generateByTypeElements(types, processingEnv);
        } catch (Exception e) {
            Tool.println(log, "ERROR %s", Tool.getStacktrace(e));
            e.printStackTrace();
        }
        Tool.close(log);
    }


    static Writer log;

    protected static void generateByVariableElements(Set<VariableElement> elements, ProcessingEnvironment processingEnv) {


        generateByElements(elements, processingEnv, new DataElementExtractor<VariableElement, ModelData>() {
            @Override
            public ModelData extract(VariableElement variableElement) {
                ModelData data = null;
                if (ElementTool.isDeclaredType(variableElement)) {
                    TypeElement typeElement = ElementTool.asTypeElement(variableElement.asType());
                    data = getModelData(typeElement, variableElement);
                }

                return data;
            }
        });


    }

    protected static void generateByTypeElements(Set<TypeElement> elements, ProcessingEnvironment processingEnv) {


        generateByElements(elements, processingEnv, new DataElementExtractor<TypeElement, ModelData>() {
            @Override
            public ModelData extract(TypeElement typeElement) {
                ModelData data = null;
                data = getModelData(typeElement, typeElement);
                return data;
            }
        });


    }

    protected static <E extends Element> void generateByElements(Set<E> elements, ProcessingEnvironment processingEnv, DataElementExtractor<E, ModelData> extractor) {


        try {

            Set<ModelData> interfaces = new HashSet<ModelData>();
            for (E e : elements) {

                ModelData data = extractor.extract(e);
                if (data != null) {
                    interfaces.add(data);
                } else {
                    processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, "The field's type must be an interface with property methods (get,set)", e);
                }

            }
            Tool.println(log, "interfaces?  with model?", interfaces.size());
            if (!interfaces.isEmpty()) {
                generateClasses(interfaces, processingEnv);
            }

        } catch (Exception e) {

            String s = Tool.getStacktrace(e);
            Tool.println(log, "mmm %s", s);
            e.printStackTrace();
        }

    }

    private static void generateClasses(Set<ModelData> interfaces, ProcessingEnvironment processingEnv) {
        for (ModelData i : interfaces) {
            implementModelInterface(i, interfaces, processingEnv);
        }

    }

    public static ModelData getModelData(TypeElement element, Element annotationOwner) {
        ModelData modelData = null;
        boolean isInterface = ElementTool.isInterface(element);
        if (isInterface) {
            modelData = new ModelData(element, annotationOwner, ModelInjection.class);
        }

        return modelData;
    }


    protected static boolean isPropertyId(JFieldVar property, ModelInjection modelInjectionAnn) {
        boolean result = false;
        if (property != null && modelInjectionAnn != null) {
            result = property.name().equals(modelInjectionAnn.propertyId());
        }
        return result;
    }

    protected static void implementModelInterface(ModelData modelData, Set<ModelData> interfaces, ProcessingEnvironment processingEnv) {

        TypeElement modelInterface = modelData.getElement();
        //Set<VariableElement> interfaces;
        List<ExecutableElement> methodElements = ElementFilter.methodsIn(modelInterface.getEnclosedElements());

        ModelInjection modelInjectionAnn = modelData.getAnnotation();


        String implementationPackage = Tool.getImplementationPackage(modelData);
        String interfaceName = modelInterface.getSimpleName().toString();
        String className = Tool.getImplementedName(implementationPackage, interfaceName, modelInjectionAnn);
        Writer code = Tool.generateClass(processingEnv, className);
        JCodeModel codeModel = new JCodeModel();
        try {
            JDefinedClass modelClass = codeModel._class(JMod.PUBLIC | JMod.FINAL, className, ClassType.CLASS);
            modelClass._implements(codeModel.ref(ElementTool.getQualifiedName(modelInterface)));
            modelClass.annotate(GeneratedCode.class);
            modelClass.annotate(DaoInjection.class);
            boolean hasId = false;
            for (ExecutableElement e : methodElements) {
                if (Tool.isGetMethod(e)) {
                    boolean addAnnotationProperty = false;
                    JFieldVar property = generateProperty(e, modelClass, codeModel);
                    boolean isId = isPropertyId(property, modelInjectionAnn);
                    boolean isForeign = Tool.isForeign(property, interfaces);
                    addAnnotationProperty = ValidDataTypes.isValidSerializableType(property.type().fullName()) || isForeign;
                    JAnnotationUse databaseFieldAnn = null;
                    if (addAnnotationProperty) {
                        databaseFieldAnn = addDatabaseFieldAnnotation(property);
                        configureDatabaseFieldAnnotation(databaseFieldAnn, isId, isForeign, modelInjectionAnn);
                    }

                    if (isForeign) {
                        String newType = Tool.getImplementedName(implementationPackage, property.type().name(), modelInjectionAnn);
                        property.type(codeModel.ref(newType));
                        configureDatabaseFieldAnnotation(databaseFieldAnn, isId, isForeign, modelInjectionAnn);
                    }

                    String propertyType = property.type().fullName();

                    Tool.println(ModelGenerator.log, "Es collections %s?", propertyType);

                    if (ValidDataTypes.isCollection(propertyType)) {
                        property.javadoc().append("This field is not stored in this persisted class.");
                        String[] typeParameters = CodeModelTool.getTypeParametersAsString(propertyType);
                        Tool.println(ModelGenerator.log, "type parameters of collections %s? r: %s", propertyType, Arrays.asList(typeParameters));
                        if(typeParameters.length>0) {
                            boolean isTypeParameterForeign = Tool.isForeign(typeParameters[0], interfaces);
                            if (isTypeParameterForeign) {
                                //Generate Relationship
                                RelationshipGenerator.generate(modelClass, typeParameters[0], implementationPackage, modelInjectionAnn, processingEnv);
                            }
                        }
                    }

                    generatePropertyMethods(e, property, modelClass, codeModel, isForeign);
                    hasId |= isId;
                }

            }
            if (!hasId) {
                Tool.generateId(modelClass, modelInjectionAnn.defaultPropertyIdType(), modelInjectionAnn, codeModel);
            }

            Tool.generateDefaultConstructor(modelClass);
            Tool.generateConstructorFields(modelClass);

            try {
                ParcelableInterfaceGenerator.generate(modelClass, modelInterface, new ParcelableReferences(modelData), codeModel);
            } catch (Throwable e) {
                Tool.println(ModelGenerator.log, "excep %s", e);
                //e.printStackTrace();
            }


        } catch (JClassAlreadyExistsException e) {
            e.printStackTrace();
        }
        Tool.build(codeModel, code);
    }




    protected static void configureDatabaseFieldAnnotation(JAnnotationUse databaseFieldAnn, boolean isId, boolean isForeign, ModelInjection modelInjectionAnn) {
        if (databaseFieldAnn != null) {
            if (isId) {
                databaseFieldAnn.param("generatedId", true);
            }

            if (isForeign) {
                databaseFieldAnn.param("foreign", true);
                databaseFieldAnn.param("foreignAutoRefresh", modelInjectionAnn.foreignAutoRefresh());
            }
        }
    }

    protected static JAnnotationUse addDatabaseFieldAnnotation(JFieldVar field) {
        return field.annotate(DatabaseField.class);
    }

    protected static JFieldVar generateProperty(ExecutableElement executableElement, JDefinedClass modelClass, JCodeModel codeModel) {
        String method = Tool.getMethodName(executableElement);
        boolean isGetMethod = Tool.isGetMethod(executableElement);
        JFieldVar field = null;
        if (isGetMethod) {
            Tool.println(log, "isGetMethod %s", method);
            String propertyName = ElementTool.getPropertyName(method);
            JClass returnType = CodeModelTool.getReturnTypeAsJClass(executableElement, codeModel);
            Tool.println(log, "propertyName %s", propertyName);
            Tool.println(log, "returnType %s", returnType);

            field = modelClass.field(JMod.PRIVATE, returnType, propertyName);

            if (ValidDataTypes.isList(returnType.fullName())) {
                String verbose = "";
                JClass arrayListRef = codeModel.ref(ArrayList.class);

                    List<String> tps = Arrays.asList(CodeModelTool.getTypeParametersAsString(returnType));

                    if(tps.size()>0) {
                        arrayListRef = arrayListRef.narrow(
                                codeModel.ref(tps.get(0)));
                    }

                Tool.println(log, "a list! %s (%s) [%s]", returnType, Arrays.asList(returnType.typeParams()), verbose);
                field.init(JExpr._new(arrayListRef));
            }
        }

        return field;
    }

    protected static void generatePropertyGetMethod(ExecutableElement executableElement, JFieldVar property, JDefinedClass modelClass, JCodeModel codeModel) {
        String methodName = Tool.getMethodName(executableElement);
        JClass returnType = CodeModelTool.getReturnTypeAsJClass(executableElement, codeModel);
        Tool.println(log, "return type of %s is %s", executableElement.getSimpleName(), returnType);
        JMethod method = null;

        method = modelClass.method(JMod.PUBLIC, returnType, methodName);
        method.body()._return(property);
    }

    protected static void generatePropertySetMethod(ExecutableElement executableElement, JFieldVar property, JDefinedClass modelClass, JCodeModel codeModel, boolean castImplementation) {
        boolean getMethodExists = Tool.containsGetPrefix(executableElement);
        String methodName = Tool.getSetMethodName(property, getMethodExists);
        JClass paramType = CodeModelTool.getReturnTypeAsJClass(executableElement, codeModel);
        Class<?> returnType = void.class;
        Tool.println(log, "returntype of %s is %s", methodName, returnType);
        JMethod method = null;

        method = modelClass.method(JMod.PUBLIC, returnType, methodName);
        JVar param = method.param(paramType, property.name());
        if (castImplementation) {
            method.javadoc().addParam(param).add(String.format("A %s implementation.", property.type().fullName()));
            method.body().assign(JExpr.refthis(property.name()), JExpr.cast(property.type(), param));
        } else {
            method.body().assign(JExpr.refthis(property.name()), param);
        }
    }

    protected static void generatePropertyMethods(ExecutableElement executableElement, JFieldVar property, JDefinedClass modelClass, JCodeModel codeModel, boolean castImplementation) {
        generatePropertyGetMethod(executableElement, property, modelClass, codeModel);
        generatePropertySetMethod(executableElement, property, modelClass, codeModel, castImplementation);
    }

    protected static void generateModel(ProcessingEnvironment processingEnv, TypeElement type) {

        String modelClassName = type.getQualifiedName().toString();
//        Writer code = Tool.generateClass(processingEnv, modelClassName.);
//
//        JCodeModel codeModel = new JCodeModel();
//        codeModel._class()
    }

    public static void print(Writer writer, Element e) {
        try {
            writer.append("instance of VariableElement " + (e instanceof VariableElement) + "\n");
            writer.append("instance of ExecutableElement " + (e instanceof ExecutableElement) + "\n");
            writer.append(e.getClass() + " " + e.getKind() + " " + e.getKind().isField() + " " + e.getKind().isInterface() + " " + e.getModifiers() + " " + e.getSimpleName() + " " + e.asType() + "\n");
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}
