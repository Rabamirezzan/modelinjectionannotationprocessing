package com.modelinjection.injection;

import com.codeinjection.tools.ElementTool;
import com.codeinjection.tools.ImplementationClassesReferences;
import com.modelinjection.injection.data.ModelData;

/**
 * Created by rramirezb on 15/01/2015.
 */
public class ParcelableReferences implements ImplementationClassesReferences {
    ModelData modelData;

    public ParcelableReferences(ModelData modelData) {
        this.modelData = modelData;
    }

    @Override
    public String getImplementation(String parcelableInterface) {
        String result = Tool.getImplementedName(
                Tool.getImplementationPackage(modelData),
                ElementTool.getSimpleClassName(parcelableInterface),
                modelData.getAnnotation()) ;
        return result;
    }
}
