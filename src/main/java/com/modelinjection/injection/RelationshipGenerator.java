package com.modelinjection.injection;

import com.codeinjection.tools.CodeModelTool;
import com.codeinjection.tools.ElementTool;
import com.j256.ormlite.field.DatabaseField;
import com.ormliteinjection.core.db.injection.DaoInjection;
import com.ormliteinjection.core.db.injection.GeneratedCode;
import com.sun.codemodel.*;

import javax.annotation.processing.ProcessingEnvironment;
import java.util.UUID;

/**
 * Created by rramirezb on 30/12/2014.
 */
public class RelationshipGenerator {
    public static void generate(JDefinedClass modelClass, String typeParameter, String packageName, ModelInjection modelInjectionAnn, ProcessingEnvironment processingEnv) {
        String typeParameterImpl = Tool.getImplementedName(packageName, ElementTool.getSimpleClassName(typeParameter), modelInjectionAnn);
        String rightRelationship = ElementTool.getSimpleClassName(typeParameterImpl);
        String leftRelationship = ElementTool.getSimpleClassName(modelClass.fullName());
        String relationshipClassName = String.format("%s.%sHas%s", packageName, leftRelationship, rightRelationship);
        JCodeModel codeModel = new JCodeModel();
        try {
            JDefinedClass relationshipClass = codeModel._class(JMod.PUBLIC | JMod.FINAL, relationshipClassName, ClassType.CLASS);
            relationshipClass.annotate(GeneratedCode.class);
            relationshipClass.annotate(DaoInjection.class);

            //addIdField(relationshipClass);
            JFieldVar fieldL = addField(relationshipClass, modelClass.fullName(), leftRelationship, modelInjectionAnn, codeModel);
            JFieldVar fieldR = addField(relationshipClass, typeParameterImpl, rightRelationship, modelInjectionAnn, codeModel);

            addBeanMethods(relationshipClass, modelClass.fullName(), leftRelationship, modelInjectionAnn, codeModel);
            addBeanMethods(relationshipClass, typeParameterImpl, rightRelationship, modelInjectionAnn, codeModel);
            // addBeanMethods(relationshipClass, UUID.class.getName(), "_id", codeModel);


            Tool.generateDefaultConstructor(relationshipClass);
            Tool.generateConstructorFields(relationshipClass, fieldL, fieldR);
            Tool.generateId(relationshipClass, modelInjectionAnn.defaultPropertyIdType(), codeModel);
            Tool.build(codeModel, Tool.generateClass(processingEnv, relationshipClassName));
        } catch (JClassAlreadyExistsException e) {
            e.printStackTrace();
        }
    }



    private static void addIdField(JDefinedClass relationshipClass) {
        JFieldVar field = relationshipClass.field(JMod.PRIVATE, UUID.class,
                "_id");
        field.annotate(DatabaseField.class).param("generatedId", true);
    }

    private static JFieldVar addField(JDefinedClass relationshipClass, String type, String className, ModelInjection modelInjectionAnn, JCodeModel codeModel) {
        JFieldVar field = relationshipClass.field(JMod.PRIVATE, codeModel.ref(CodeModelTool.getTypeWithoutTypeParameters(type)),
                ElementTool.toLowerFirstLetter(className));
        field.annotate(DatabaseField.class).param("foreign", true).param("uniqueCombo", true).param("canBeNull", false);//(.param("foreignAutoRefresh",modelInjectionAnn.foreignAutoRefresh());
        return field;
    }

    private static void addBeanMethods(JDefinedClass relationshipClass, String type, String className, ModelInjection modelInjectionAnn, JCodeModel codeModel) {
        String propertyName = ElementTool.toLowerFirstLetter(className);
        String methodPartName = ElementTool.toLowerFirstLetter(className);
        JMethod setMethod = relationshipClass.method(JMod.PUBLIC, void.class, !modelInjectionAnn.suppressPOJOPrefixes()?Tool.getSetMethodName(methodPartName):methodPartName);
        JVar param = setMethod.param(codeModel.ref(type), propertyName);
        setMethod.body().assign(JExpr.refthis(propertyName), param);

        JMethod getMethod = relationshipClass.method(JMod.PUBLIC, codeModel.ref(type), !modelInjectionAnn.suppressPOJOPrefixes()?Tool.getGetMethodName(methodPartName):methodPartName);

        getMethod.body()._return(JExpr.refthis(propertyName));

    }
}
