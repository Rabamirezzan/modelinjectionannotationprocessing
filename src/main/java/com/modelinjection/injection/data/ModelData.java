package com.modelinjection.injection.data;

import com.codeinjection.tools.data.DataTypeElement;
import com.modelinjection.injection.ModelInjection;

import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;

/**
 * Created by rramirezb on 06/01/2015.
 */
public class ModelData extends DataTypeElement<ModelInjection> {


    public ModelData(TypeElement element, Element annotationOwner, Class<ModelInjection> annotationClass) {
        super(element, annotationOwner, annotationClass);
    }
}
