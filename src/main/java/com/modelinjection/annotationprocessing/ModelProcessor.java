package com.modelinjection.annotationprocessing;


import com.modelinjection.injection.ModelInjection;
import com.modelinjection.injection.ModelGenerator;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by rramirezb on 26/12/2014.
 */
//@SupportedAnnotationTypes("com.opesystems.modelinjection.injection.ModelInjection")
public class ModelProcessor extends AbstractProcessor {

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return new HashSet<String>(Arrays.asList(ModelInjection.class.getName()));
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        if(roundEnv.processingOver()||annotations.isEmpty()){
            return false;
        }
        Set<? extends Element> elements = roundEnv.getElementsAnnotatedWith(ModelInjection.class);

        ModelGenerator.generate(processingEnv, elements);
        return true;
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }
}
